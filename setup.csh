# ! /usr/bin/env bash

# Setup PYTHONPATH and install necessary python packages
# @TODO add option to call each group separatelly
export PYTHONPATH=~/workspace/ohm_assessment

# Setup necessary libs for installing other Python libs
sudo apt-get install python3 python-dev python3-dev build-essential libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev python-pip
pip install PyYAML

# Install main list of libs for project
pip install -r requirements.txt

# at this point databases shall be created(empty?)
SAMPLE_CONFIG=config/my_development.cnf.sample
# Prepare development
cp -rf $SAMPLE_CONFIG config/my_development.cnf  # db name is ohm_assessment
export FLASK_ENVIRONMENT=development             # this is only part of config name (after _)
echo "Setting up the development database"
mysql -u root -p ohm_assessment < config/seed.sql # @TOOD get variables from .cnf file
alembic upgrade head                             # if this is failing, check FLASK_ENVIRONMENT and database name

# Prepare my_test(all same except database name)
cp -rf $SAMPLE_CONFIG config/my_test.cnf
sed -i 's/\(database =\).*/\1 my_test/' config/my_test.cnf  # db name is my_test
export FLASK_ENVIRONMENT=test               # this is only part of config name (after _)
echo "Setting up the my_test database"
mysql -u root -p my_test < config/seed.sql  # @TOOD get variables from .cnf file
alembic upgrade head                        # if this is failing, check FLASK_ENVIRONMENT and database name
